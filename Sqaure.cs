using System;

namespace Square
{
    class Square
    {
        private int sideSize;

        public int SideSize { 
            get => sideSize; 
            set => sideSize = value; 
        }

        public void DrawEmpty(char character){
            for (int i = 1; i <= SideSize; i++)
            {
                for (int j = 1; j <= SideSize; j++)
                {
                    if(i == 1 || i == SideSize){
                        System.Console.Write(character);
                        continue;
                    }
                    if(j == 1 || j == SideSize){
                        System.Console.Write(character);
                    }else{
                        System.Console.Write(" ");
                    }
                }
                System.Console.WriteLine();
            }
        }

        public void DrawFull(char character){
            for (int i = 1; i <= SideSize; i++)
            {
                for (int j = 1; j <= SideSize; j++)
                {
                    System.Console.Write(character);
                }
                System.Console.WriteLine();  
            }
        }
    }
}
